# Roommate

## Overview

This application will be use to track your expenses along with other people in your flat

## Checklist

- [x] dockerfile
- [x] react front-end
- [ ] unittests
- [x] go backend
- [ ] clean architecture
- [ ] terraform
- [ ] GCP
- [ ] firestore
- [x] local development