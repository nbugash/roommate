package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/nbugash/roommate/expense/api/openapi"
	"gitlab.com/nbugash/roommate/expense/repository"
	"gitlab.com/nbugash/roommate/expense/router"
	"gitlab.com/nbugash/roommate/expense/service"
	"log"
	"os"
	"strconv"
)

var port = 0

func init() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Unable to load .env file")
	}
	port, err = strconv.Atoi(os.Getenv("REST_API_PORT"))
	if err != nil {
		log.Fatalf("Port not specified %v", err)
	}
}

func main() {
	repo := repository.NewPgRepository(true)
	srv := service.NewExpenseService(repo)
	api := openapi.NewExpenseController(srv)
	expenseAPIRouter := router.NewMuxRouter("/expenses", api)
	expenseAPIRouter.Serve(port)
}
