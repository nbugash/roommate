# Roommate

## Overview

This application will be use to track your expenses along with other people in your flat

## Checklist

- [x] dockerfile
- [x] react front-end
- [ ] unittests
- [x] go backend
- [ ] clean architecture
- [ ] terraform
- [ ] GCP
- [ ] firestore
- [x] local development

## Installing Expense API and WebApp frontend

1. Initialized the  using the Makefile
    ```bash
    make devSetup
    ```
2. Fill `.env` and `.env.docker` with the proper entries
3. Build artifacts and executable
   ```bash
   make
   ```
4. Run the web application
   ```bash
   ./expense/main
   ```
5. Click [here](http://localhost:8080) open the web application

## Infrastructure

### Building the API docker image
```bash
export DOCKER_IMG=expense-tracker-api; docker build -t $DOCKER_IMG .
```
or
```bash
make dockerBuildImg
```

### Spinning the Postgres database
Use `docker-compose.yml` to spin up a local instance of postgres. The command is
```bash
export PROJECT_NAME=expense-tracker; docker-compose -f docker-compose.yml -p $PROJECT_NAME up -d roommateDB
```
or
```bash
make runDatabase
```

## Local Development

### Expense Micro-service
To run the expense micro service, first you need to ensure that the expenseDB container is already running.
```bash
make runDatabase
```

Next, build the executable
```bash
make apiBuild
```

Lastly, run the executable
```bash
./expense/server
```