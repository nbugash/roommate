package openapi

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/nbugash/roommate/expense/entity"
	"gitlab.com/nbugash/roommate/expense/service"
)

//Controller interface
type Controller interface {
	GetExpense(res http.ResponseWriter, req *http.Request)
	GetExpenses(res http.ResponseWriter, req *http.Request)
	CreateExpense(res http.ResponseWriter, req *http.Request)
	DeleteExpense(res http.ResponseWriter, req *http.Request)
	UpdateExpense(res http.ResponseWriter, req *http.Request)
	DeleteExpenses(res http.ResponseWriter, req *http.Request)
}

type expenseCtr struct {
	srv service.ExpenseService
}

//NewExpenseController controller
func NewExpenseController(srv service.ExpenseService) Controller {
	return &expenseCtr{
		srv: srv,
	}
}

func (ec *expenseCtr) CreateExpense(res http.ResponseWriter, req *http.Request) {
	var expense entity.Expense
	err := json.NewDecoder(req.Body).Decode(&expense)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(`{"error":"Unable to unmarshal the expense from the request"}`))
		return
	}
	_, err = ec.srv.ValidateExpense(&expense)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(`{"error":"Expense data received is invalid."}`))
		return
	}
	result, err := ec.srv.CreateExpense(expense)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	res.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(res).Encode(result)
	if err != nil {
		log.Fatalf("Unable to marshal expense %v", err)
		return
	}
}

func (ec *expenseCtr) DeleteExpense(res http.ResponseWriter, req *http.Request) {
	id := mux.Vars(req)["id"]
	expense, err := ec.srv.DeleteExpense(id)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(`{error: "Unable to delete expense id:` + id + ` "}`))
		return
	}
	if expense == nil {
		res.WriteHeader(http.StatusNotFound)
		res.Header().Set("Content-Type", "application/json")
		res.Write([]byte(`{"data":[]}`))
		return
	}
	res.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(res).Encode(expense)
	if err != nil {
		log.Fatalf("Unable to marshal expense: %v", err)
		return
	}
}

func (ec *expenseCtr) GetExpense(res http.ResponseWriter, req *http.Request) {
	id := mux.Vars(req)["id"]
	expense, err := ec.srv.GetExpense(id)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(`{error: "Unable to delete expense id:` + id + ` "}`))
		return
	}
	if expense == nil {
		res.WriteHeader(http.StatusNotFound)
		res.Header().Set("Content-Type", "application/json")
		res.Write([]byte(`{"data":[]}`))
		return
	}
	res.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(res).Encode(expense)
	if err != nil {
		log.Fatalf("Unable to marshal expense: %v", err)
		return
	}

}

func (ec *expenseCtr) GetExpenses(res http.ResponseWriter, req *http.Request) {
	expenses := ec.srv.GetExpenses()
	if expenses == nil {
		res.WriteHeader(http.StatusNotFound)
		res.Header().Set("Content-Type", "application/json")
		res.Write([]byte(`{"data":[]}`))
		return
	}
	res.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(res).Encode(expenses)
	if err != nil {
		log.Fatalf("Unable to marshal expense %v", err)
		return
	}

}

func (ec *expenseCtr) UpdateExpense(res http.ResponseWriter, req *http.Request) {
	id := mux.Vars(req)["id"]
	var expense entity.Expense
	err := json.NewDecoder(req.Body).Decode(&expense)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(`{"error":"Unable to unmarshal the expense from the request"}`))
		return
	}
	err = ec.srv.UpdateExpense(id, &expense)
	if err != nil {
		res.WriteHeader(http.StatusNotFound)
		res.Write([]byte(`{"data":[]}`))
		return
	}
	res.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(res).Encode(expense)
	if err != nil {
		log.Fatalf("Unable to marshal expense: %v", err)
		return
	}
}

func (ec *expenseCtr) DeleteExpenses(res http.ResponseWriter, req *http.Request) {
	err := ec.srv.DeleteExpenses()
	if err != nil {
		res.WriteHeader(http.StatusNoContent)
		return
	}
}
