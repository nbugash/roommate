package router

// Router interface
type Router interface {
	Serve(port int)
}
