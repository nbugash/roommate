package router

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/mux"
	"gitlab.com/nbugash/roommate/expense/api/openapi"
)

var muxRouter = mux.NewRouter()

type muxDispatcher struct {
	ctr openapi.Controller
}

type spaHandler struct {
	staticPath string
	indexPath  string
}

func (sh spaHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	// get the absolute path to prevent directory traversal
	path, err := filepath.Abs(request.URL.Path)
	if err != nil {
		// if we failed to get the absolute path respond with a 400 bad request
		// and stop
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	// prepend the path with the path to the static directory
	path = filepath.Join(sh.staticPath, path)

	// check whether a file exists at the given path
	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		// file does not exist, serve index.html
		http.ServeFile(writer, request, filepath.Join(sh.staticPath, sh.indexPath))
		return
	} else if err != nil {
		// if we got an error (that wasn't that the file doesn't exist) stating the
		// file, return a 500 internal server error and stop
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	// otherwise, use http.FileServer to serve the static dir
	http.FileServer(http.Dir(sh.staticPath)).ServeHTTP(writer, request)
}

//NewMuxRouter returns a mux router instance
func NewMuxRouter(uri string, ctr openapi.Controller) Router {
	muxRouter.HandleFunc(uri+"/{id}", ctr.GetExpense).Methods("GET")
	muxRouter.HandleFunc(uri+"/{id}", ctr.DeleteExpense).Methods("DELETE")
	muxRouter.HandleFunc(uri, ctr.GetExpenses).Methods("GET")
	muxRouter.HandleFunc(uri, ctr.CreateExpense).Methods("POST")
	muxRouter.HandleFunc(uri, ctr.DeleteExpenses).Methods("DELETE")
	muxRouter.HandleFunc(uri, ctr.UpdateExpense).Methods("UPDATE")

	log.Println("Loading SPA")

	spa := spaHandler{
		staticPath: "../webapp/build",
		indexPath:  "index.html",
	}
	muxRouter.PathPrefix("/").Handler(http.FileServer(http.Dir(spa.staticPath)))
	return &muxDispatcher{
		ctr: ctr,
	}
}

func (*muxDispatcher) Serve(port int) {
	log.Printf("Server started listening on port: %d", port)
	http.ListenAndServe(fmt.Sprintf(":%d", port), muxRouter)
}
