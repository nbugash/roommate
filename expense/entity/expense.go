package entity

// Expense entity
type Expense struct {
	ID          string  `json:"id"`
	Title       string  `json:"title"`
	Amount      float64 `json:"amount"`
	Description string  `json:"description"`
	CreatedAt   string  `json:"created_at"`
	CreatedBy   int64   `json:"created_by"`
	ModifiedBy  int64   `json:"modified_by"`
	UpdatedAt   string  `json:"updated_at"`
}
