package service

import (
	"errors"
	"log"

	"gitlab.com/nbugash/roommate/expense/entity"
	"gitlab.com/nbugash/roommate/expense/repository"
)

type service struct {
	repo repository.ExpenseRepository
}

// ExpenseService handles all business logic
type ExpenseService interface {
	ValidateExpense(expense *entity.Expense) (bool, error)
	CreateExpense(expense entity.Expense) (*entity.Expense, error)
	GetExpenses() []entity.Expense
	GetExpense(id string) (*entity.Expense, error)
	DeleteExpense(id string) (*entity.Expense, error)
	UpdateExpense(id string, expense *entity.Expense) error
	DeleteExpenses() error
}

// NewExpenseService service implementation
func NewExpenseService(repo repository.ExpenseRepository) ExpenseService {
	return &service{
		repo: repo,
	}
}

// ValidateExpense perform sanity check on the expenses
func (srv *service) ValidateExpense(expense *entity.Expense) (bool, error) {
	if expense.Title == "" {
		return false, errors.New("Title can not be empty")
	} else if expense.Description == "" {
		return false, errors.New("Description can not be empty")
	}
	return true, nil
}

func (srv *service) CreateExpense(expense entity.Expense) (*entity.Expense, error) {
	_, err := srv.ValidateExpense(&expense)
	if err != nil {
		log.Fatalf("Unable to create expense: %v", err)
	}
	return srv.repo.Create(expense)

}

func (srv *service) GetExpenses() []entity.Expense {
	expenses, err := srv.repo.GetAll()
	if err != nil {
		return nil
	}
	return expenses
}

func (srv *service) GetExpense(id string) (*entity.Expense, error) {
	return srv.repo.Get(id)
}

func (srv *service) DeleteExpense(id string) (*entity.Expense, error) {
	return srv.repo.Delete(id)
}

func (srv *service) UpdateExpense(id string, expense *entity.Expense) error {
	_, err := srv.ValidateExpense(expense)
	if err != nil {
		log.Fatalf("Unable to update expense: %v", err)
		return err
	}
	srv.repo.Update(id, expense)
	return nil
}

func (srv *service) DeleteExpenses() error {
	return srv.repo.DeleteAll()
}
