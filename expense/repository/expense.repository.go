package repository

import "gitlab.com/nbugash/roommate/expense/entity"

// ExpenseRepository uses repository pattern to retreive data for the expense entity
type ExpenseRepository interface {
	Get(id string) (*entity.Expense, error)
	GetAll() ([]entity.Expense, error)
	Create(expense entity.Expense) (*entity.Expense, error)
	Update(id string, expense *entity.Expense) (*entity.Expense, error)
	Delete(id string) (*entity.Expense, error)
	DeleteAll() error
}
