package repository

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	// Postgres driver
	_ "github.com/lib/pq"
	"gitlab.com/nbugash/roommate/expense/entity"
)

//PgConn postgres connecton struct
type PgConn struct {
	dbConn *sql.DB
}

func initDB(db *sql.DB) {
	log.Println("Creating expenses table if it does not exists")

	const createExpenseTable = `CREATE TABLE IF NOT EXISTS expenses (
		id SERIAL,
		title VARCHAR,
		description VARCHAR,
		amount FLOAT,
		created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
		created_by INT,
		updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
		modified_by INT
	)
	`
	if _, err := db.Query(createExpenseTable); err != nil {
		log.Fatalf("Unable to create a new expenses table")
	}
}

// NewPgRepository Constructor for creating a db connection
func NewPgRepository(emptyTable bool) ExpenseRepository {
	connStr := fmt.Sprintf("user=%s password=%s host=%s port=%s database=%s sslmode=disable",
		os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PWD"), os.Getenv("POSTGRES_HOSTNAME"),
		os.Getenv("POSTGRES_PORT"), os.Getenv("POSTGRES_DATABASE"),
	)

	//Open connection
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}

	log.Println(connStr)
	err = db.Ping()
	if err != nil {
		log.Fatalln("Unable to connecto to the database")
	}

	log.Println("Successfully connected to the database")
	initDB(db)
	return &PgConn{
		dbConn: db,
	}
}

//Get returns an expense entitry given an id
func (pgConn *PgConn) Get(id string) (*entity.Expense, error) {
	var expense entity.Expense
	row, err := pgConn.dbConn.Query(`SELECT * FROM expenses WHERE id=$1`, id)
	defer row.Close()
	if err != nil {
		return nil, err
	}
	if row.Next() {
		// unmarshal the row object to expense
		err = row.Scan(&expense.ID, &expense.Title, &expense.Description,
			&expense.Amount, &expense.CreatedAt, &expense.CreatedBy,
			&expense.UpdatedAt, &expense.ModifiedBy)
		switch err {
		case sql.ErrNoRows:
			log.Println("No rows were returned")
			return &expense, nil
		case nil:
			return &expense, nil
		default:
			log.Fatalf("Unable to scan the row for any expense: %v", err)
		}
		return &expense, err
	}
	return nil, nil
}

//GetAll retrieve all expenses from the expenses table
func (pgConn *PgConn) GetAll() ([]entity.Expense, error) {
	var expeneses []entity.Expense
	rows, err := pgConn.dbConn.Query(`SELECT * FROM expenses`)
	defer rows.Close()
	if err != nil {
		log.Printf("Unable to find any expenses from the database")
		return expeneses, err
	}
	for rows.Next() {
		var expense entity.Expense
		err = rows.Scan(&expense.ID, &expense.Title, &expense.Description,
			&expense.Amount, &expense.CreatedAt, &expense.CreatedBy,
			&expense.UpdatedAt, &expense.ModifiedBy)
		if err != nil {
			log.Fatalf("Unable to scan the row %v", err)
		}
		// append expense to the expenses list
		expeneses = append(expeneses, expense)
	}
	return expeneses, err

}

//Create save expense into the database
func (pgConn *PgConn) Create(expense entity.Expense) (*entity.Expense, error) {
	row, err := pgConn.dbConn.Query(`INSERT INTO expenses (title, description, amount, created_by, 
		modified_by) VALUES ($1, $2, $3, $4, $5) RETURNING id, created_at, updated_at AS expense`, expense.Title, expense.Description, expense.Amount,
		expense.CreatedBy, expense.ModifiedBy)
	defer row.Close()
	if err != nil {
		log.Printf("Unable to INSERT data into the table")
		return nil, err
	}
	for row.Next() {
		if err = row.Scan(&expense.ID, &expense.CreatedAt, &expense.UpdatedAt); err != nil {
			log.Fatalf("Unable to scan row, %v", err)
		}
	}
	return &expense, err

}

//Update save an updated version of the expense into the database
func (pgConn *PgConn) Update(id string, expense *entity.Expense) (*entity.Expense, error) {
	row, err := pgConn.dbConn.Query(`UPDATE expenses SET title=$2, description=$3, amount=$4, created_at=$5, created_by=$6
		updated_at=NOW(), modified_by=$7 WHERE id=$1 RETURNING id`, expense.ID, expense.Title, expense.Description, expense.Amount,
		expense.CreatedAt, expense.CreatedBy, expense.ModifiedBy)
	defer row.Close()
	if err != nil {
		log.Printf("Unable to update table with %v", expense)
		return nil, err
	}
	return expense, err

}

//Delete remove an expense from the database
func (pgConn *PgConn) Delete(id string) (*entity.Expense, error) {
	row, err := pgConn.dbConn.Query(`DELETE FROM expenses WHERE id=$1 RETURNING *`, id)
	if err != nil {
		log.Fatalf("Unable to delete expense id: %s", id)
	}
	var expenese entity.Expense
	if row.Next() {
		err = row.Scan(&expenese.ID, &expenese.Title, &expenese.Description, &expenese.Amount, &expenese.CreatedAt,
			&expenese.CreatedBy, &expenese.UpdatedAt, &expenese.ModifiedBy)
		if err != nil {
			log.Fatal("Unable to scan row when deleting expense")
		}
		if expenese.ID == "" {
			log.Fatal("No data found to delete")
			return nil, nil
		}
		return &expenese, err
	}
	return nil, nil
}

//DeleteAll removes all expenses from the table
func (pgConn *PgConn) DeleteAll() error {
	_, err := pgConn.dbConn.Query(`DELETE FROM expenses`)
	if err != nil {
		return err
	}
	return nil

}
